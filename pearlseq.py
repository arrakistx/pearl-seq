#!/usr/bin/env python3

""" 
Initializes the a run of the PEARL-seq pipeline, which tabulated mutations or stalls (or both).

Designed for Python >=3.6. Backward compatibility is untested.

Currently, the only required input is the directory that the raw data and metadata files. The following files should be 
placed in this directory, but the filenames can also be overwritten with specific arguments.

./raw_data/  <-- Contains all the raw FASTQ files
./input_sheet.csv  <-- Spreadsheet that describes how the experiments are grouped
./ref_seqs.fa  <-- FASTA file that contains all the reference sequences
./ref_masks.fa  <-- Contains reference sequence masks, with masked nucleotides replaced with "x" or "X"
(This can just be a copy of ref_seqs.fa if there is no mask)

See the example_data_*/ folders within the pearlseq directory for an example dataset.

"""

from __future__ import print_function, division

__pipeline__ = 'pearl-seq'
__version__ = '0.1'
__author__ = 'Lee Vandivier'
__license__ = 'GPLv3'

# Load standard modules
import argparse
import sys
import re
from glob import glob
from os import path, makedirs, symlink, environ
from multiprocessing import cpu_count
from subprocess import Popen, PIPE
from time import sleep

# Load non-standard modules
try:
    import json
    import pandas as pd
    from Bio import SeqIO
    from snakemake import snakemake
    from datetime import datetime
    from src.filehandling import load_dataframe
except ModuleNotFoundError as e:
    print(e)
    print('===========')
    print('Failed to find a required module. Check the conda install commands within the Dockerfile')
    exit()
    

# Get location of pipeline directory
pipeline_dir = path.dirname(path.realpath(__file__))

# compile regex
integer = re.compile('^\d+$')

###################
# Parse arguments #
###################

argparser = argparse.ArgumentParser(description="""
                                                PEARL-seq stalls is a software package for analysis of PEARL-seq
                                                data. For more information, visit: https://bitbucket.org/arrakistx/XXXXX
                                                """,
                                                formatter_class=argparse.ArgumentDefaultsHelpFormatter)

#Inputs and outputs
io_args = argparser.add_argument_group('Inputs and outputs options')
io_args.add_argument('input_dir', default = '0_raw', help='Directory where input FASTQ files are found')
io_args.add_argument('output_dir', default = '1_pearlseq', help='Directory where output files will be created')
io_args.add_argument('--input_sheet', default = 'input_sheet.csv', help='Input spreadsheet, if not found in standard location')
io_args.add_argument('--ref_fa', default = 'ref_seqs.fa', help='FASTA file containing reference sequences')
io_args.add_argument('--mask_file', default = 'ref_masks.fa', help='FASTA file describing masked positions in reference sequences')

#Read proprocessing steps
preprocessing_args  = argparser.add_argument_group('Read preprocessing options')

#Read processing and alignment steps
alignment_args = argparser.add_argument_group('Read alignment options')
alignment_args.add_argument('--pear_params', '-pp', default = "", nargs = '+', type = str, help='String of extra parameters for PEAR stitcher.')
alignment_args.add_argument('--align_method', help='Alignment method (available: bowtie2, bowtie2-endToEnd)', default='bowtie2', choices=['bowtie2', 'bowtie2-endToEnd'])
alignment_args.add_argument('--sort_memory', help='Memory to allocated to samtools sort. Warning: will be multiplied by number of processors for final memory consumption. Needed for very large samples (>25G fastq) in which number of sort files exceeds system limit for opened files (typically 1024)', default='768M')
alignment_args.add_argument('--unique', help='retain only uniquely mapping reads (map q > 10)', action='store_true')

# Mutation tabulation steps
map_args = argparser.add_argument_group('Mutation profiling options')
map_args.add_argument('--count', default = "mutations", choices = ["mutations", "stalls", "both"], help='Types of evidence to count when tabulating reactivity')
map_args.add_argument('--min_coverage', help='Minimum read coverage to calculate reactivities at a given position', type=int, default=1000)
map_args.add_argument('--max_mut_rate', help="""Positions with mutation rates greater than this fraction will be filtered out (0-1).""", type=float, default=0.2)
map_args.add_argument('--max_ctrl_mut_rate', help="""Positions with mutation rates greater than this fraction will be filtered out (0-1).""", type=float, default=0.02)
map_args.add_argument('--max_indel_length', help="""Maximum length of indels to consider for reactivity calculations. Indels with length > this value will be discarded.""", type=int, default=10)
map_args.add_argument('--min_read_length', help="""Minimum read length after trimming by base quality.""", type=int, default=30)
map_args.add_argument('--use_only_pairs', help='Only use read pairs where both mates survive trimming and QC.', action='store_true')
map_args.add_argument('--skip_insertions', help='Do not use insertions when calculating reactivity profiles.', action='store_true')
map_args.add_argument('--skip_deletions', help='Do not use deletions when calculating reactivity profiles.', action='store_true')


#Snakemake generic parameters
snakemake_args = argparser.add_argument_group('Snakemake options')
snakemake_args.add_argument('--num_cores', help='Number of cores to use (default: all available)', type=int, default=cpu_count())
snakemake_args.add_argument('--overwrite',
                    help='Overwrite final output files even if precurors unchanged.', action='store_true')
snakemake_args.add_argument('--dryrun', help='Snakemake dry run', action='store_true') 
snakemake_args.add_argument('--force_incomplete', help='Snakemake force overwrite incomplete files', action='store_true') 
snakemake_args.add_argument('--unlock', help='Snakemake unlock directory only', action='store_true') 
snakemake_args.add_argument('--processors', help="Maximum number of processors to use when multiprocessing is available", type=int, default=1)
snakemake_args.add_argument('--keep_temp', help="Keep temp files", action = 'store_true')


args = argparser.parse_args()

#Validate arguments
if not path.exists(args.ref_fa):
    raise Exception(f'Reference sequence file not found at: {args.ref_fa}')
if not path.exists(args.mask_file):
    raise Exception(f'Reference mask file not found at: {args.mask_file}. \
                     This can just be a copy of the reference sequence file')
if not 0 <= args.max_mut_rate <= 1:
    raise Exception('--max_mut_rate must be a value between 0 and 1.')
if not 0 <= args.max_ctrl_mut_rate <= 1:
    raise Exception('--max_ctrl_mut_rate must be a value between 0 and 1.')


#parse arguments of arguments (metaargs) if provided. Otherwise specify defaults
def parse_metaarg(metaarg:list):
    string = ' '.join(metaarg)
    string = " " + re.sub("^\s", "", string)
    return string

pear_params = parse_metaarg(args.pear_params)


####################
# Set target flags #
####################

#parse pipeline flags
trimFlag = "_trimmomatic"

alignFlag = args.align_method

uniqueFlag = ""
if args.unique:
    uniqueFlag = "_uniqueMappers"

stitchFlag = "_stitched"

countFlag = ""
if args.count == "mutations":
    countFlag = "_mut-bam-readcount"
elif args.count == "stalls":
    countFlag = "_stall"
elif args.count == "both":
    countFlag = "stallAndMut-bam-readcount"

###############################
# Verify presence of raw data #
###############################

def parse_ref_seqs(ref_seq_string, ref_seqs_from_fasta):
    """Parses the "Reference sequence" field in the input spreadsheet."""

    if ref_seq_string == '*':
        ref_list = list(ref_seqs_from_fasta.keys())

    elif ',' in ref_seq_string:
        ref_list = ref_seq_string.split(',')

    elif ';' in ref_seq_string:
        ref_list = ref_seq_string.split(';')
    else:
        ref_list = [ref_seq_string]

    for seq_name in ref_list:
        if seq_name not in ref_seqs_from_fasta:
            raise Exception('Sequence {} provided in input spreadsheet is not in reference FASTA file.'.format(seq_name))

    return ref_list

def validate_name(name):
    if '_' in name:
        raise Exception('pearlseq does not support underscores in sample names.\
            Please replace them in sample {}.'.format(name))
    if name != '*':
        if not re.match(r'^[a-zA-Z0-9][A-Za-z0-9-]*$', name):
            print('-- Sample names should only include alphanumeric characters and dashes, or a single "*". --')
            raise Exception('Problematic sample name: {}'.format(name))

def parse_sample_metadata(row, sample, name, fastq_dict, ref_seq_dict, treatment_var, treatment_dict):

    ### parse ref_seq and treatment associations ###
    ref_seq_dict[name] = parse_ref_seqs(row['Reference sequence'], ref_seqs_from_fasta)
    if name not in treatment_dict:
        treatment_dict[name] = []
    # treatment_dict[name] = row[treatment_var]
    treatment_dict[name].append(row[treatment_var])

    ### parse fastqs associations ###
    all_fastq_files = sorted(glob(path.join(args.input_dir, '**/*{}_*.fastq.gz'.format(sample)), recursive=True))
    all_fastq_files = list(filter(lambda x:'unpaired.fastq.gz' not in x, all_fastq_files))

    # Try to match by sample name
    matching_fastq_files = [_ for _ in all_fastq_files if re.search('[/_]' + sample, _)]
    
    # If that fails, try to use the sample name as a potential sample number
    if len(matching_fastq_files) == 0:
        matching_fastq_files = [_ for _ in all_fastq_files if re.search('_' + sample + '_', _)]

    if len(matching_fastq_files) != 2:
        print(' '.join(matching_fastq_files))
        print(row)
        raise Exception('Wrong number of matching FASTQ files for sample.')

    assert '_R1' in matching_fastq_files[0]
    assert '_R2' in matching_fastq_files[1]
    assert matching_fastq_files[0].replace('_R1', '_R0') == matching_fastq_files[1].replace('_R2', '_R0')

    fastq_dict[name] = tuple(matching_fastq_files) 


# Check that sample_df is properly formatted
sample_df = load_dataframe(args.input_sheet)

if 'Treatment' in list(sample_df):
    treatment_var = 'Treatment'
elif 'Ligand' in list(sample_df):
    treatment_var = 'Ligand'
else:
    raise Exception('Did not find a column specifying treatment or ligand')

assert sum([bool(integer.match(i)) for i in sample_df.columns]) == 0, "Sample sheet columns names cannot be integers"
assert sum([bool(integer.match(i)) for i in list(sample_df[treatment_var])]) == 0, "Sample sheet treatment names cannot be integers"

# Read reference sequences  

ref_seqs_from_fasta = SeqIO.to_dict(SeqIO.parse(args.ref_fa, 'fasta'))

if len(ref_seqs_from_fasta) == 0:
    raise Exception('Provided reference FASTA contains no sequences.')

# Identify the raw FASTQ files that match each sample and match the samples to their reference sequence
# More generally, this gathers metadata pertaining to specific samples, such as the presence of a treatment/ligand 
# and associated reference sequences.

fastq_dict, ref_seq_dict, treatment_dict, probe_dict = {}, {}, {}, {}

#If sample_df has both Name and Sample columns, rename output from Sample->Name. If only one, keep name/sample the same 
hasName = 'Name' in sample_df.columns
hasSample = 'Sample' in sample_df.columns

for _, row in sample_df.iterrows():

    if hasName and hasSample:
        name = row['Name']
        sample = row['Sample']
    elif hasSample: 
        name = sample = row['Sample']
    elif hasName:
        name = sample = row['Name']

    validate_name(name)
    parse_sample_metadata(row, sample, name, fastq_dict, ref_seq_dict, treatment_var, treatment_dict)
    probe_string = row['Probe']
    if probe_string in ['probe', 'Probe', 'NAI', 'nai', 'EDC', 'edc', 'LASER', 'laser', 'DMS', 'dms', 'NAZ', 'naz']:
        probe_dict[name] = 'probe'
    elif probe_string in ['control', 'Control', 'ctrl', 'Ctrl', 'DMSO', 'dmso', "Dmso", "PBS", "Pbs", "pbs"]:
        probe_dict[name] = 'control'
    else:
        raise Exception('Probe id {probe_string} not recognized in row:\n-----\n {row}'.format(probe_string = probe_string, row = row))
    
names = list(fastq_dict.keys())
all_ref_seq_names = list(set([_ for name in ref_seq_dict for _ in ref_seq_dict[name]]))

# Read blackout masks, if available
mask_dict = {}
if path.exists(args.mask_file):
    mask_fa_dict = SeqIO.to_dict(SeqIO.parse(args.mask_file, 'fasta'))

    for seq_name in mask_fa_dict:
        masked_pos_list = [i  for i, _ in enumerate(mask_fa_dict[seq_name].seq) if _ in ['x', 'X']]

        # Because R is weird about empty vectors, add a dummy mask with position -1
        if len(masked_pos_list) == 0:
            masked_pos_list = [-1]

        mask_dict[seq_name] = masked_pos_list

######################
## Prep config file ##
######################

def parse_fa_lengths(fasta):
    lengths = {}
    fa_handle = SeqIO.parse(fasta, 'fasta')
    for record in fa_handle:
        name = str(record.description)
        length = len(str(record.seq))
        lengths[name] = length
    return lengths

lengths_dict = parse_fa_lengths(args.ref_fa)
lengths_dict = {i: lengths_dict[i] for i in all_ref_seq_names} #only print lengths if a refseq is associated with at least one sample 



config_dict = {'version': __version__, 'input_sheet': path.relpath(args.input_sheet, args.output_dir),
                'ref_fa': path.basename(args.ref_fa), 
                'ref_masks': path.basename(args.mask_file),
                'ref_seq_dict': json.dumps(ref_seq_dict), 
                'ref_seq_lengths': json.dumps(lengths_dict),
                'treatment_dict': json.dumps(treatment_dict), 
                'probe_dict': json.dumps(probe_dict), 
                'names':names,
                'trimming_method': "trimmomatic", 
                'pear': pear_params,
                'mapping_method': args.align_method, 
                'count': args.count, 
                'min_coverage': args.min_coverage, 
                'max_mut_rate': args.max_mut_rate, 
                'max_ctrl_mut_rate': args.max_ctrl_mut_rate,
                'max_indel_length': args.max_indel_length, 
                'min_read_length': args.min_read_length,
                'skip_insertions': args.skip_insertions, 
                'skip_deletions': args.skip_deletions, 
                'unique_mappers_only': args.unique, 
                'read_stitching': True, 
                'processors': args.processors, 
                'sort_memory': args.sort_memory, 
                'keep_temp': args.keep_temp, 
                'mask_dict': json.dumps(mask_dict),
                'read_files_to_use': ['RS']
                }

############################
## Prep output + santized ##
############################

# Create a folder with symlinks to FASTQ files in a way that they are now named consistently with sample names. Also symlink to reference files

if not path.isdir(args.output_dir):
    makedirs(args.output_dir)

sanitized_fastq_dir = path.join(args.output_dir, 'sanitized')
if not path.isdir(sanitized_fastq_dir):
    makedirs(sanitized_fastq_dir)

for name in fastq_dict:
    r1_file, r2_file = fastq_dict[name]
    new_r1_file = path.join(sanitized_fastq_dir, '{}_R1.fastq.gz'.format(name))
    if not path.exists(new_r1_file):
        symlink(path.relpath(r1_file, sanitized_fastq_dir), new_r1_file)
    new_r2_file = path.join(sanitized_fastq_dir, '{}_R2.fastq.gz'.format(name))
    if not path.exists(new_r2_file):
        symlink(path.relpath(r2_file, sanitized_fastq_dir), new_r2_file)
    fastq_dict[name] = (new_r1_file, new_r2_file)

new_ref_fa = path.join(args.output_dir, path.basename(args.ref_fa))
new_mask_file = path.join(args.output_dir, path.basename(args.mask_file))
if path.exists(args.ref_fa) and not path.exists(new_ref_fa):
    symlink(path.relpath(args.ref_fa, args.output_dir), new_ref_fa)
if path.exists(args.mask_file) and not path.exists(new_mask_file):
    symlink(path.relpath(args.mask_file, args.output_dir), new_mask_file)


################################
# Run analysis using Snakemake #
################################

# Define per-sample targets and generate prerequisite files using Snakemake

fastq_dict, ref_seq_dict, treatment_dict, probe_dict

per_sample_mut_rates = []
for name in fastq_dict:
   per_sample_mut_rates.append(f"output/per_sample_mutation_rates/{alignFlag}{stitchFlag}{trimFlag}{uniqueFlag}{countFlag}/{name}_mut_rate.txt.gz")

per_ref_seq_agg_mut_rates = []
per_ref_seq_agg_mut_plots = []
for ref_seq_name in all_ref_seq_names:
    per_ref_seq_agg_mut_rates.append(f"output/mutation_rates/{alignFlag}{stitchFlag}{trimFlag}{uniqueFlag}{countFlag}_minCov{args.min_coverage}/{ref_seq_name}_agg_mut_rate.txt")
    per_ref_seq_agg_mut_rates.append(f"output/mutation_rates/{alignFlag}{stitchFlag}{trimFlag}{uniqueFlag}{countFlag}_minCov{args.min_coverage}/{ref_seq_name}_ctrlAgg_mut_rate.txt")
    per_ref_seq_agg_mut_plots.append(f"output/mutation_rates/{alignFlag}{stitchFlag}{trimFlag}{uniqueFlag}{countFlag}_minCov{args.min_coverage}/{ref_seq_name}_agg_mut_rate.mutation-FC.pdf")

all_targets = per_sample_mut_rates + per_ref_seq_agg_mut_rates + per_ref_seq_agg_mut_plots


success = snakemake(path.join(pipeline_dir, 'src/Snakefile'), targets=all_targets, forcetargets=args.overwrite, workdir=args.output_dir, cores=args.num_cores, dryrun=args.dryrun, force_incomplete = args.force_incomplete, unlock = args.unlock, notemp = args.keep_temp, config=config_dict)

# Write parameters to a JSON dump file

with open(path.join(args.output_dir, 'params.json'), 'w') as out_handle:
    json.dump(config_dict, out_handle)

print('======================')

