![Logo](../img/PEARLseq-logo.png)

***
# Input files for PEARL-seq Stalls

### Raw data

Place raw data in single directory (e.g. `0_raw`). The folder structure doesn't need to be 'flat', and files can be detected even if nested in subdirectories (e.g. as is the case for Illumina raw data). Raw data are then soft-linked to a `sanitized` directory in the output folder.

For now, let's assume you created a directory called **my_shape_data/**

#### Naming input fastq.gz files

PEARL-seq Stalls expects paired end reads, which are then stitching together to unambiguously define fragment termini.
Sample names follow the [Illumina FASTQ naming convention](https://support.illumina.com/help/BaseSpace_OLH_009008/Content/Source/Informatics/BS/NamingConvention_FASTQ-files-swBS.htm), with some built-in flexibility. Mate1 must contain `_R1` and mate2 must contain `_R2` in the file name. Files are searched using a prefix, such as `Apt21-DMSO-Cpd2d-None-1` to find the two files below:
- Apt21-DMSO-Cpd2d-None-1_S37_L001_R1_001.fastq.gz
- Apt21-DMSO-Cpd2d-None-1_S37_L001_R2_001.fastq.gz


#### Config file: input_sheet.csv (or .xlsx)

This is a spreadsheet that describes how sequence read files (FASTQ files) relate to reference sequence and to the presence or absence of PEARL-seq probes. Sample names are used to find fastq.gz files in the raw data directory. Below is an example:

|Experiment name|Replicate|Reference sequence|Ligand|Sample|Probe
|Aptamer21-Cpd2d|1|Aptamer21|Cpd2d|Apt21-DMSO-Cpd2d-None-1|probe
|Aptamer21-Cpd2d|2|Aptamer21|Cpd2d|Apt21-DMSO-Cpd2d-None-2|probe
|Aptamer21-Cpd2d|3|Aptamer21|Cpd2d|Apt21-DMSO-Cpd2d-None-3|probe
|Aptamer21-Cpd2d|1|Aptamer21|Cpd2d|Apt21-DMSO-DMSO-None-1|control
|Aptamer21-Cpd2d|2|Aptamer21|Cpd2d|Apt21-DMSO-DMSO-None-2|control
|Aptamer21-Cpd2d|3|Aptamer21|Cpd2d|Apt21-DMSO-DMSO-None-3|control

** Important: ** Sample names must only include alphanumeric characters and dashes. This is because Snakemake relies
on wildcards in filenames. You can also enter the sample numbers (S1, S2, S3) into the spreadsheet instead if this is an issue.

It is important that the name provided in column C ("Reference sequence") exactly matches the reference sequence names provided
in the reference FASTA file (see below).

#### File: example_data_stalls/ref_seqs.fa

A simple FASTA file that contains the sequences of the RNAs assayed in each PEARL-seq experiment. They can be provided as either DNA or RNA.

For the Aptamer21 example, this is the file:

        >Aptamer21
        AGGGCCTTCGGGCCAAGGGTAGGCCAGGCAGCCAACTAGCGAGAGCTTAAATCTCTGAGCCCGAGAGGGTTCAGTGCTGCTTATGTGGACGGCTTTCGATCCGGTTCGCCGGATCCAAATCGGGCTTCGGTCCGGTTC

Each row in the spreadsheet must be matched (by name/header) to one of the reference sequences described in the FASTA file.
Otherwise, this will create an error.

#### File: example_data_stalls/ref_masks.fa

A simple FASTA file that contains the sequences of the RNAs assayed in each PEARL-seq experiment, with positions that should
be masked (i.e., positions for which SHAPE values will not be used for folding and will be highlighted as masked in the plots)
replaced with an X.

For the Aptamer21 riboswitch example, this is the file:

        >Aptamer21
        AGGGCCTTCGGGCCAAGGGTAGGCCAGGCAGCCAACTAGCGAGAGCTTAAATCTCTGAGCCCGAGAGGGTTCAGTGCTGCTTATGTGGACGGCTTTCGATCCGGTTCGCCGGATCCAAATXXXXXXXXXXXXXXXXXX


Note that the first few bases have been replaced by "X" characters. This is because the experiment was done with a primer
that anneals to that sequence, reducing the apparent mutation rate and leading to non-representative RT stall values.
The same approach can be used to indicate bases in
a sequence for which the PEARL-seq data is not reliable due to a high background stall rate or other issues with reverse transcription,
PCR, or sequencing.

As before, each row in the spreadsheet must be matched (by name/header) to one of the reference sequences described in the FASTA file.
Otherwise, this will create an error.

### Running analyze_shape-map.py

Once these files are in place, PEARLseq Stalls can  be run in place with the following commands (with paths adjusted for your system):

        pearlseq --unique --count stalls --input_sheet input_sheet_apt21.Cpd2d.csv --ref_fa aptamer21.fa --mask_file aptamer21_mask.fa --align_method bowtie2 0_raw 1_pearlseq
***