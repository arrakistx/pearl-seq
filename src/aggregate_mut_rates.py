#!/usr/bin/env python3

###
__script__='aggregate_mut_rates.py'
__version__='0.1'
# __author__ = 'Lee Vandivier'

import norm
import numpy as np

################################################################################################################################


################################################################################################################################

if __name__ == "__main__":

	agg, ctrl_agg, masks = norm.norm(snakemake.input.mut_rate_files, snakemake.params.treatment_dict, snakemake.params.probe_dict, snakemake.params.masks, snakemake.wildcards.seq, coverage_thresh=snakemake.params.min_cov, ref=None, positions=None, mask_bases=None, conditions=None, clip=0.001, cmr_thresh = snakemake.params.max_ctrl_mut_rate)
	agg.replace('', np.nan)
	agg.to_csv(snakemake.output.agg, sep='\t', index=None, na_rep = 'NaN')
	ctrl_agg.replace('', np.nan)
	ctrl_agg.to_csv(snakemake.output.ctrl_agg, sep='\t', index=None, na_rep = 'NaN')