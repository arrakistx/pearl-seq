#!/usr/bin/env python3

###
__script__='sum_stalls_and_muts.py'
__version__='0.1'
__author__ = 'Lee Vandivier'

import gzip

#################

def sum_stalls_and_muts(stalls_file, muts_file, output_file):
	output_FH = gzip.open(output_file, 'wb')
	with gzip.open(stalls_file, 'r') as stalls_FH, gzip.open(muts_file, 'r') as muts_FH: 
		for stall, mut in zip(stalls_FH, muts_FH):
			(stall_target, stall_position, stall_base, stall_coverage, stall_rate, stall_rate_amb) = stall.decode().rstrip().split("\t")[:6]
			(mut_target, mut_position, mut_base, mut_coverage, mut_rate, mut_rate_amb, discarded_deletions, discarded_insertions) = mut.decode().rstrip().split("\t")
			
			# Stall (pysam count_coverage) and mutation (bam-readcount) coverages will not be equal if there are indels. Pysam does not count indels toward coverarge while bam-readcount does
			# assert stall_target == mut_target and stall_position == mut_position and stall_base == mut_base and float(stall_coverage) == float(mut_coverage), f"targets, positions, base, and coverage do not match for mutations and stalls.\nStalls: {stall}\nMutations: {mut}"
			assert stall_target == mut_target and stall_position == mut_position and stall_base == mut_base, f"targets, positions, and base do not match for mutations and stalls.\nStalls: {stall}\nMutations: {mut}"
			stalls = round(float(stall_rate) * float(stall_coverage))
			stalls_amb = round(float(stall_rate_amb) * float(stall_coverage))
			muts = round(float(mut_rate) * float(mut_coverage))
			muts_amb = round(float(mut_rate_amb) * float(mut_coverage))
			if float(mut_coverage) > 0:
				total_rate = float(stalls + muts) / float(mut_coverage)
				total_rate_amb = float(stalls_amb + muts_amb) / float(mut_coverage)
			else: 
				total_rate = 0
				total_rate_amb = 0
			# since coverage is constant, can directly sum rates
			# total_rate = float(stall_rate) + float(mut_rate)
			# total_rate_amb = float(stall_rate_amb) + float(mut_rate_amb)

			output_FH.write(f"{stall_target}\t{stall_position}\t{stall_base}\t{stall_coverage}\t{total_rate}\t{total_rate_amb}\t{discarded_deletions}\t{discarded_insertions}\n".encode('utf-8'))

################################################################################################################################

if __name__ == "__main__":

	sum_stalls_and_muts(snakemake.input.stall, snakemake.input.mut, snakemake.output[0])
