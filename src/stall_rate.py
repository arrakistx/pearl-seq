#!/usr/bin/env python3

###
__script__='stall_rate.py'
__version__='0.3' #adding unstranded option (strand == both)
# Need to benchmark for +/- strand and unstitched paired-end
__author__ = 'Lee Vandivier'

#benchmarked against `bedtools genomecov -5 -d` for single-end reads

import sys, re, argparse, gzip
from Bio import SeqIO
import pysam
import numpy as np

#################

def load_fasta_to_dict(fasta):
	targets = {}
	seqs = SeqIO.parse(fasta, 'fasta')
	for seq in seqs:
		targets[seq.name] = seq.seq
	return targets

def load_regions(regions):
	#regions file is 1-based start and stop
	#convert to 0-based inclusive starts, 0-based exclusive / 1-based stops per pysam and python slices 
	starts = {}
	stops = {}
	with open(regions) as regionsFH:  
		for line in regionsFH:
			(target, start, stop) = line.rstrip().split("\t")
			starts[target] = int(start) - 1
			stops[target] = int(stop)
	return (starts, stops)

def readFivePrimeEndCoverage_se(input_file, contig, start, stop, strand):
	#start is 0-based, stop is 1-based 
	infile = pysam.AlignmentFile(input_file, "rb")
	if not start:
		start = 0
	if not stop:
		stop = infile.lengths[infile.references.index(contig)]
	coverage_dict = dict.fromkeys(range(start,stop), 0)
	#
	if strand == "+":
		for read in infile.fetch(contig, start, stop):
			if not read.is_reverse:
				five_end = read.reference_start
				if start <= five_end <= stop-1: #not all reads in interval will start in interval
					coverage_dict[five_end] += 1
	elif strand == "-":
		for read in infile.fetch(contig, start, stop):
			if read.is_reverse:
				five_end = read.reference_end - 1 #reference_end is open-end / 1-based
				if start <= five_end <= stop-1:
					coverage_dict[five_end] += 1
	elif strand == "both":
		for read in infile.fetch(contig, start, stop):
			if not read.is_reverse:
				five_end = read.reference_start
			else: 
				five_end = read.reference_end - 1
			if start <= five_end <= stop-1: #not all reads in interval will start in interval
				coverage_dict[five_end] += 1		
	infile.close()
	coverage = [i for i in coverage_dict.values()]
	return coverage 

def readCoverage_se(input_file, contig, start, stop, strand):
	def filter_plus_reads(read):
		if not read.is_reverse:
			return True
	def filter_minus_reads(read):
		if read.is_reverse:
			return True
	infile = pysam.AlignmentFile(input_file, "rb")
	if strand == "+":
		(A,C,G,T) = infile.count_coverage(contig, start, stop, quality_threshold = 20, read_callback = filter_plus_reads) #returns tuple of per-base coverage
	elif strand == '-':
		(A,C,G,T) = infile.count_coverage(contig, start, stop, quality_threshold = 20, read_callback = filter_minus_reads)
	elif strand == 'both':
		(A,C,G,T) = infile.count_coverage(contig, start, stop, quality_threshold = 20)
	coverage = [sum(x) for x in zip(A,C,G,T)]
	return coverage

def readFivePrimeEndCoverage_to_adductCoverage(coverage, offset = 1):
	adducts =  coverage[offset:] + [0] * offset
	return adducts

def calc_stall_rate(input, output, targets_fa, region_file, mode):
	targets = load_fasta_to_dict(targets_fa)
	(starts, stops) = load_regions(region_file)
	outputFH = gzip.open(output, 'wb')
	for target in targets.keys():
		assert target in starts	and target in stops, f"Unable to find target {target} in region file {region_file}"
		start = starts[target]
		stop = stops[target]
		sequence = targets[target]
		five_end_cov = readFivePrimeEndCoverage_se(input, target, start, stop, 'both') #None, None fetches entire contig
		read_cov = readCoverage_se(input, target, start, stop, 'both')
		assert len(read_cov) == len(five_end_cov), "Unequal length five_end_cov and read_cov"
		assert len(read_cov) == len(sequence), f"Sequence is of length {len(sequence)} but read coverages of length {len(read_cov)}"
		adduct_cov = readFivePrimeEndCoverage_to_adductCoverage(five_end_cov)
		if mode == 'perBase':
			stall_rate = np.array(adduct_cov) / (np.array(read_cov) + np.array(adduct_cov))
		elif mode == 'perTotal':
			stall_rate = np.array(adduct_cov) / sum(np.array(adduct_cov))
		stall_rate = np.nan_to_num(stall_rate)
		for index, (base, coverage, rate) in enumerate(zip(sequence,read_cov, stall_rate)):
			position = index + 1 #keep consistent with 1-based positions from Shapeware
			outputFH.write(f"{target}\t{position}\t{base}\t{coverage}\t{rate}\t{rate}\t0\t0\n".encode('utf-8'))
	outputFH.close()

################################################################################################################################

if __name__ == "__main__":

	calc_stall_rate(snakemake.input.bam, snakemake.output[0], snakemake.input.fa, snakemake.input.region, snakemake.params.mode)

