""" Processes the output of bam-readcount to estimate mutation rates as a prelude to calculating SHAPE
reactivities.

This is mean to be used by snakemake rather than as a standalone.

"""

import gzip

class MutationProfile():

    def __init__(self, name, dna_seq):
    
        import numpy as np

        self.seq = dna_seq
        self.name = name
        self.length = len(self.seq)
        self.seq_range = range(0, self.length)

        self.coverage = np.zeros(self.length)
        self.mut_counts = np.zeros(self.length)

        self.amb_mut_counts = np.zeros(self.length)
        self.amb_coverage = np.zeros(self.length)

        self.discard_dict = {'+': np.zeros(self.length), '-': np.zeros(self.length)}


    def count_snv(self, offset, base, base_counts):
        """Count a single-nucleotide variant that is presumably unambiguous."""

        assert self.seq[offset] != base
        # Make sure that we are not counting mutations that are equal to the reference

        self.mut_counts[offset] += base_counts
        self.coverage[offset] += base_counts


    def adjust_del_pos(self, offset, variant):
        """This function controls where mutations get counted relative to their aligment.

        In this case, we assume that the adduct was found in the ((first non-mutated position)) -> first mutated position at 3' end. 
        """ 
       	adj_offset = offset + len(variant) -1

        # Make sure the adjustment does not extend beyond the length of the sequence? Or handle later?
        return adj_offset  



    def count_deletion(self, offset, variant, variant_counts):

        potential_offsets = self.find_ambiguous_del_positions(offset, variant)

        if len(potential_offsets) == 1:  #Unambiguous case

            adj_offset = self.adjust_del_pos(potential_offsets[0], variant)

            if adj_offset in self.seq_range:
                self.mut_counts[adj_offset] += variant_counts

            return 'Unambiguous'

        else:

            for amb_offset in potential_offsets:

                adj_amb_offset = self.adjust_del_pos(amb_offset, variant)

                if adj_amb_offset in self.seq_range:

                    self.amb_mut_counts[adj_amb_offset] += variant_counts
                    self.amb_coverage[adj_amb_offset] += variant_counts

            return 'Ambiguous'


    def count_insertion(self, offset, variant, variant_counts, flank_size):

        potential_offsets = self.find_ambiguous_ins_positions(offset, variant, flank_size)

        if len(potential_offsets) == 1:  #Unambiguous case

            if potential_offsets[0] in self.seq_range:
                self.mut_counts[potential_offsets[0]] += variant_counts

            return 'Unambiguous'

        else:

            for amb_offset in potential_offsets:

                if amb_offset in self.seq_range:

                    self.amb_mut_counts[amb_offset] += variant_counts
                    self.amb_coverage[amb_offset] += variant_counts


            return 'Ambiguous'


    def find_ambiguous_del_positions(self, offset, variant):

        var_length = len(variant)
        start_indices = (offset, offset + var_length)

        assert self.seq[start_indices[0]: start_indices[1]] == variant

        amb_positions = [offset]

        # Left search 

        test_index_start, test_index_end = start_indices
        while test_index_start >= 0:
            test_index_start -= 1
            test_index_end -= 1

            if self.seq[test_index_start: test_index_end] == variant:
                amb_positions.insert(0, test_index_start)
            elif test_index_start <= amb_positions[0] - var_length:
                break

        # Right search

        test_index_start, test_index_end = start_indices
        while test_index_end <= self.length - 1:
            test_index_start += 1
            test_index_end += 1

            if self.seq[test_index_start: test_index_end] == variant:
                amb_positions.append(test_index_start)
            
            elif test_index_start >= amb_positions[-1] + var_length:
                break

        return amb_positions


    def find_ambiguous_ins_positions(self, offset, variant, flank_size):

        from Bio import pairwise2

        var_length = len(variant)
        start_indices = (offset + 1, offset + var_length + 1)

        search_region_start = max(start_indices[0] - flank_size, 0)
        search_region_end = min(start_indices[1] + flank_size, len(self.seq))

        seq_with_ins = self.seq[search_region_start:offset + 1] + variant + self.seq[offset + 1:search_region_end]

        potential_alns = pairwise2.align.globalxx(seq_with_ins, self.seq[search_region_start: search_region_end])

        ins_mask = ''.join(['-' for _ in range(var_length)])

        amb_positions = []

        for aln in potential_alns:
            if ins_mask in aln[1]:
                amb_positions.append(aln[1].index(ins_mask) + search_region_start)

        return amb_positions


    def print_profile(self):
        print(self.seq)
        print('Coverage')
        print(self.coverage)
        print('Unambiguous coverage')
        print(self.amb_coverage)

        for base, mut, amb_mut in zip(self.seq, self.mut_counts, self.amb_mut_counts):
            print(base, mut, amb_mut, sep='\t')


        print('===============')


def calculate_mutation_rate(bam_count_file, ref_file, rate_out_file, mut_out_file, sample, config_dict):

    from Bio import SeqIO
    import json
    
    bases_to_count = set(['A', 'C', 'G', 'T'])

    rate_out_handle = gzip.open(rate_out_file, 'w')
    mut_out_handle = gzip.open(mut_out_file, 'w')

    fa_handle = SeqIO.to_dict(SeqIO.parse(ref_file, 'fasta'))

    ref_seq_names = json.loads(config_dict['ref_seq_dict'])[sample]
    max_indel_length = config_dict['max_indel_length']


    mut_profiles = {}

    for ref_seq_name in ref_seq_names:
        mut_profiles[ref_seq_name] = MutationProfile(ref_seq_name, str(fa_handle[ref_seq_name].seq))


    for line in open(bam_count_file, 'r'):
        fields = line.strip().split('\t')    
        seq_name = fields[0]

        if seq_name not in mut_profiles:
            continue
        else:
            mut_profile = mut_profiles[seq_name]


        pos = int(fields[1]) - 1  # Array is 0-based, but data coming in are 1-based
        ref_base = fields[2]
        coverage = float(fields[3])

        mut_profile.coverage[pos] += coverage

        variants = fields[4:]

        for variant in variants:
            variant_stats = variant.split(':')
            base_call = variant_stats[0]
            base_count = float(variant_stats[1])

            # Case #1: single nucleotide variant that is different from reference

            if base_call in bases_to_count and base_call != ref_base:
                mut_profile.mut_counts[pos] += base_count
                # mut_out_handle.write("\t".join([str(i) for i in [seq_name, pos + 1, pos + 1, ref_base, base_call, base_count, coverage, 'Unambiguous']])+"\n")
                mut_out_handle.write(f"{seq_name}\t{pos + 1}\t{pos + 1}\t{ref_base}\t{base_call}\t{base_count}\t{coverage}\tUnambiguous\n".encode('utf-8'))

            # Case #2: deletion

            elif base_call[0] == '-':

                if config_dict['skip_deletions']:
                    continue

                del_bases = base_call[1:]    

                if len(del_bases) <= max_indel_length:

                    mut_type = mut_profile.count_deletion(pos, del_bases, base_count)    

                    mut_out_handle.write(f"{seq_name}\t{pos + 1}\t{pos + 1 + len(del_bases)}\t{ref_base}\t{base_call}\t{base_count}\t{coverage}\t{mut_type}\n".encode('utf-8'))

                    if mut_type == 'Ambiguous':
                        mut_profile.discard_dict['-'][pos] += base_count    

            # Case #3: insertion

            elif base_call[0] == '+':

                if config_dict['skip_insertions']:
                    continue

                ins_bases = base_call[1:]    

                if len(ins_bases) <= max_indel_length:

                    mut_type = mut_profile.count_insertion(pos, ins_bases, base_count, 2 * len(ins_bases))

                    mut_out_handle.write(f"{seq_name}\t{pos + 1}\t{pos + 1}\t{ref_base}\t{base_call}\t{base_count}\t{coverage}\t{mut_type}\n".encode('utf-8'))

                    if mut_type == 'Ambiguous':
                        mut_profile.discard_dict['+'][pos] += base_count

    # Calculate the actual mutation rate

    for ref_seq_name in mut_profiles:

        mut_profile = mut_profiles[ref_seq_name] 

        for pos in range(mut_profile.length):
            
            ref_base = mut_profile.seq[pos]
            coverage = mut_profile.coverage[pos]

            if coverage > 0:
                mut_rate = mut_profile.mut_counts[pos] / coverage
                amb_mut_rate = (mut_profile.mut_counts[pos] + mut_profile.amb_mut_counts[pos]) / coverage
            else:
                mut_rate = 0
                amb_mut_rate = 0

            # rate_out_handle.write("\t".join([str(i) for i in [ref_seq_name, pos + 1, ref_base, coverage, mut_rate, amb_mut_rate, mut_profile.discard_dict['-'][pos], mut_profile.discard_dict['+'][pos],]])+'\n')
            rate_out_handle.write(f"{ref_seq_name}\t{pos + 1}\t{ref_base}\t{coverage}\t{mut_rate}\t{amb_mut_rate}\t{mut_profile.discard_dict['-'][pos]}\t{mut_profile.discard_dict['+'][pos]}\n".encode('utf-8'))


    rate_out_handle.close()
    mut_out_handle.close()



if __name__ == '__main__':

    calculate_mutation_rate(snakemake.input[0], snakemake.input[1], snakemake.output[0], snakemake.output[1], snakemake.wildcards['sample'], snakemake.config)
