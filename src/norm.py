import pandas as pd
import numpy as N
import re, os, json

def norm(reactivity_files, treatment_dict, probe_dict, mask, sequence, coverage_thresh=10000, ref=None, positions=None, mask_bases=None, conditions=None, clip=0.001, cmr_thresh = 0.02):
    """
    Aggregate *stall_rate.txt.gz files into one large data frame

    Takes the following parameters:

          reactivity_files: stall_rate files, usually found in temp/mutation_rates/bowtie2/*stall_rate.txt.gz
          mask            : List of positions (real, 1-based) that are masked in the experiment, eg from ref_masks.fa
          sequence        : The name of the reference sequence this applies to
          coverage_thresh : norm will auto-mask bases that do not meet this

    Returns aggregate data frame, control-only aggregate data frame, and the mask used after coverage thresholding is applied
    """

    #dict arguments are passed as strings
    treatment_dict = json.loads(treatment_dict)
    probe_dict = json.loads(probe_dict)
    mask = json.loads(mask)


    treatments = list(set().union(*treatment_dict.values()))
    if conditions:
        assert set(conditions).issubset(treatments), "Specified conditions {} are not all present in experimental conditions {}".format(conditions, treatments)
        treatments = conditions

    #load each reactivity file, ignore positions with coverage < coverage_thresh
    data = []
    ctrl_data = []
    for fn in reactivity_files:
        #load
        name = os.path.basename(fn).split("_")[0]
        r = pd.read_csv(fn, sep='\t', header=None)
        r.columns   = ['Sequence', 'Position', 'Base', 'Coverage', 'Mutation.Rate', 'Amb.Mutation.Rate', 'Del.Discard.Count', 'Ins.Discard.Count']

        #sequences/positions of interest only
        if isinstance(sequence, list):
            r = r[r['Sequence'].isin(sequence)]
        else:
            r = r[r['Sequence'] == sequence]
        if positions is not None:
            r = r[r['Position'].isin(positions)]
        if r.empty:
            print("Warning: no positions in sequence '{}' for sample '{}'".format(sequence, name))
            continue

        #calc mutation rates, then mask if coverage < coverage_thresh
        stall_rate    = N.clip(r['Mutation.Rate'], clip, 1-clip) # Logit "pseudocount"
        r['Mutation.Rate'] = stall_rate.values     
        mut_logit   = N.log(stall_rate / (1 - stall_rate))
        r['Mutation.Logit.Raw'] = mut_logit
        r.loc[r.Coverage < coverage_thresh, 'Mutation.Rate'] = N.nan
        r.loc[r.Coverage < coverage_thresh, 'Mutation.Rate.Raw'] = N.nan

        #merge to data or ctrl data
        for treatment in treatment_dict[name]:
            r['Sample'] = name
            r['Cond'] = treatment
            if probe_dict[name] == 'probe':
                data.append(r.copy()) #copy to avoid pointing when multiple treatments exist per sample 
            elif probe_dict[name] == 'control':
                ctrl_data.append(r.copy())
    
    ctrl_rate  = {}
    ctrl_rate_masked  = {}    

    for treatment in treatments:
        ctrl_data_treatment =  pd.concat([ r for r in ctrl_data if r['Cond'].unique()[0] in treatment], 0)
        ctrl_data_treatment['Mutation.Rate.Masked'] = ctrl_data_treatment['Mutation.Rate']

        ctrl_data_treatment.loc[ctrl_data_treatment['Mutation.Rate'] >= cmr_thresh, 'Mutation.Rate.Masked'] = N.nan

        ctrl_rate[treatment]  = ctrl_data_treatment.groupby('Position').mean()['Mutation.Rate'].values
        ctrl_rate_masked[treatment]  = ctrl_data_treatment.groupby('Position').mean()['Mutation.Rate.Masked'].values

    for r in data:
        c = r['Cond'].unique()[0]
        r['Control.Mutation.Rate'] = ctrl_rate[c]

        r['Mutation.FC']     = r['Mutation.Rate'] / ctrl_rate_masked[c] - 1
        r['Mutation.Log2FC'] = N.log2(r['Mutation.FC']+1)
        

    bmask = set()
    if mask_bases is not None:
        bmask = set(sum([ list(r[r['Base'].isin(mask_bases)]['Position'].unique()) for r in data+ctrl_data ], []))
    mask = set(mask) | bmask
    
    return pd.concat(data, 0), pd.concat(ctrl_data, 0), mask

