#!/usr/bin/env python3

###
__script__='plot_mut_rates.py'
__version__='0.1'
# __author__ = 'Lee Vandivier'

import matplotlib, itertools
matplotlib.use('agg')
from matplotlib import pyplot as plt

import pandas as pd
import numpy as N
import seaborn as sns
import os
import re
import json


################################################################################################################################

def draw_sd(agg, ctrl_agg, conditions, controls=None, quant=False, positions=None, react_column='Reactivity', coverage_line=10000, cmr_thresh = 0.02):

    #assert stat_data_column in ['Mutation.Logit', 'Logit.Norm']

    if positions is None:
        positions = sorted(agg['Position'].unique())

    numrows  = 4
    allconds = conditions

    axes = []
    
    if len(allconds) <= len(sns.color_palette()):
        colord = dict(zip(allconds, sns.color_palette()))
    else:
        colord = {x: '0.2' for x in allconds}

    plt.close('all')
    sns.set(color_codes=True)
    sns.set_style('dark')
    fig = plt.figure(figsize=(36, numrows*5))

    ax = fig.add_subplot(numrows,1,1)
    d = agg[agg['Cond'].isin(allconds)]
    if quant:
        d = quantile(agg, allconds, react_column)
        d = pd.DataFrame(d.stack(), columns=[react_column])
        d.reset_index(inplace=True)
    sns.lineplot(data=d, x='Position', y=react_column, hue='Cond', ci='sd', ax=ax, hue_order=allconds)
    ax.set_title('Mutation frequency (%s)' % react_column, fontsize=16)
    ax.set_ylabel('Norm mutation freq (+/- SD)')
    ax.set_ylim(-1, 8)
    if react_column in ('Reactivity', 'Mutation.FC'):
        ax.axhline(y=0.85, color='r', linestyle='--')
    ax.axhline(y=0, color='0.2', linestyle='--', alpha=0.7)
    axes.append(ax)

    #plot control mutation rate
    ax = fig.add_subplot(numrows,1,numrows-2)
    ax.axhline(y=cmr_thresh, color='m')
    sns.lineplot(data=agg, x='Position', y='Control.Mutation.Rate', hue='Cond', ci='sd', ax=ax, hue_order=allconds)
    ax.set_title('Background mutation rate', fontsize=16)
    ax.set_ylabel('Mutation frequency')
    axes.append(ax)

    if controls:
        ax = fig.add_subplot(numrows,1,3)
        ax.axhline(y=0, color='k')
        sns.lineplot(data=data, x='Position', y='Logit.FC', hue='Cond', ci='sd', ax=ax, hue_order=allconds)
        ax.set_title('Mutation frequency vs control (%s)' % stat_data_column, fontsize=16)
        ax.set_ylabel('Mutation frequency vs control mean (+/- SD)')
        for c in conditions:
            fc = data[data['Cond'] == c]['Data.Col.FC'].dropna().values
            ax.axhline(y=fc.mean() + fc.std()*z_thresh, linestyle='--', alpha=0.7, color=colord[c])
            ax.axhline(y=fc.mean() - fc.std()*z_thresh, linestyle='--', alpha=0.7, color=colord[c])

        #ax.set_ylim(-2, 2)
        axes.append(ax)

    #plot coverage
    ax = fig.add_subplot(numrows,1,numrows-1)
    sns.lineplot(data=agg[agg['Cond'].isin(allconds)], x='Position', y='Coverage', hue='Cond', ci='sd', ax=ax, hue_order=allconds)
    ax.set_title('Coverage (probe)', fontsize=16)
    ax.set_ylabel('Log Count')
    ax.set_yscale('log')
    ax.axhline(y=coverage_line, color='r', linestyle='--')
    axes.append(ax)

    #plot control coverage
    ax = fig.add_subplot(numrows,1,numrows)
    sns.lineplot(data=ctrl_agg[ctrl_agg['Cond'].isin(allconds)], x='Position', y='Coverage', hue='Cond', ci='sd', ax=ax, hue_order=allconds)
    ax.set_title('Coverage (control)', fontsize=16)
    ax.set_ylabel('Log Count')
    ax.set_yscale('log')
    ax.axhline(y=coverage_line, color='r', linestyle='--')
    axes.append(ax)

    if controls:
        for c in conditions:
            pos = data[(data['Cond'] == c) & (data['significant'] == True)]['Position'].unique()
            for ax in axes:
                for p in list(pos):
                    ax.axvline(x=p, linestyle='--', alpha=0.1, color='k')
    for ax in axes:
        ax.set_xticks(positions)
        base_labels = ax.set_xticklabels([ agg[agg['Position'] == i]['Base'].values[0] for i in positions ])
        na = set(agg[agg['Cond'].isin(allconds)][N.isnan(agg[agg['Cond'].isin(allconds)][react_column])]['Position'].unique())
        for i in range(len(positions)):
            if positions[i] in na:
                base_labels[i].set_color('#DDDDDD')
                base_labels[i].set_alpha(0.7)
        ax.set_xlim(min(positions), max(positions))
        ax.set_xlabel('')
        newax = ax.twiny()
        newax.patch.set_visible(False)
        newax.yaxis.set_visible(False)
        for spinename, spine in newax.spines.items():
            spine.set_visible(False)
        newax.set_xticks([ x for x in positions if x % 100 == 0 ])
        newax.set_xlim(min(positions), max(positions))
        newax.xaxis.set_ticks_position("bottom")
        newax.xaxis.set_label_position("bottom")
        newax.spines['bottom'].set_position(('axes', -0.15))

    plt.tight_layout()
    
    if controls:
        return data, fig
    return fig

################################################################################################################################

if __name__ == "__main__":

    treatment_dict = json.loads(snakemake.params.treatment_dict)
    treatments = list(set().union(*treatment_dict.values()))
    agg = pd.read_csv(snakemake.input.agg, sep = '\t')
    ctrl_agg = pd.read_csv(snakemake.input.ctrl_agg, sep = '\t')
    figure = draw_sd(agg, ctrl_agg, treatments, controls=None, quant=False, positions=None, react_column='Mutation.FC', coverage_line=snakemake.params.min_cov)
    figure.savefig(snakemake.output.mutation_FC, format = 'pdf')


