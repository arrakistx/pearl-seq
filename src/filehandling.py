#!/usr/bin/env python3

"""
General filehandling modules
"""

from os import path
import pandas as pd

################################################################################

def load_dataframe(sample_sheet_file:str):

        # Check if exists #
        if not path.isfile(sample_sheet_file):
                raise Exception('Input spreadsheet not found at: {}'.format(sample_sheet_file))

        # Load #
        if sample_sheet_file.endswith('.xlsx') or sample_sheet_file.endswith('.xls'):
                sample_df = pd.read_excel(sample_sheet_file)
        elif sample_sheet_file.endswith('.csv'):
                sample_df = pd.read_csv(sample_sheet_file)

        # Check if nonempty #
        if len(sample_df) == 0:
                raise Exception('Input spreadsheet is empty or corrupted.')

        return(sample_df)

