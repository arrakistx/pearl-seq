#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

mkdir $DIR/0_raw


while IFS= read -r file
do
	curl $file -o $DIR/0_raw/$(basename $file)
	if [ $? != 0 ]; then
		wget $file -P $DIR/0_raw/
	fi
	
done < $DIR/files_to_download.txt