
![Logo](img/PEARLseq-logo.png)

***

Welcome to the repository for PEARL-seq!

## Installation

##### Requirements

PEARL-seq is based on Arrakis' SHAPEware pipeline and can tabulate either mutations (Shape-MaP, DMS-MaP, etc) or stalls (Shape-Seq, PEARL-seq, etc). It is based in Docker, and requires the following software to be pre-installed on your computing environment:

- The [Docker](https://docs.docker.com/) container system
- The "git" command line tool

If you need help installing any of these tools, see the [installation instructions](docs/installation.md). When installing dependencies, make sure you
agree with the corresponding licenses of various software tools.


## Download and install PEARLseq

The easiest way to get the PEARLseq software is to clone our repository. This will ensure you always have access to the
latest version. You will need to enter both your username and password

        git clone https://bitbucket.org/arrakistx/pearlseq.git

After cloning our repository, run the included installation script:

        cd pearlseq
        ./install.sh

This will install a Docker container and alias `pearlseq` that calls this container. This step will create a separate, stable, reproducible environment in which to run analysis jobs and will not affect other software that you have installed on your system.


## Test PEARL-seq with example data

Verify your installation of PEARL-seq Stalls by analyzing Aptamer21 example data. You can find example input files in the example_data/
directory within the repository.

If you didn't already download the example files with the installation script, run:

        ./example_data_stalls/download_files.sh
        ./example_data_mutations/download_files.sh

Now, try running the PEARL-seq on the example data. By default, this will use all available processors, but this
can be configured with the --num_cores option.

        cd example_data_stalls
        pearlseq 0_raw 1_pearlseq --unique --count stalls
        cd ../example_data_mutations
        pearlseq 0_raw 1_shapemap --unique --count mutations




Once the script has finished running, you can find the output from the software in the example_data_stalls/1_pearlseq/output/ folder.

## General use

PEARL-seq stalls is designed to analyze batches of samples, such as multiplexed experiments coming from the same sequencing run
or replicate experiments for the same RNA in the presence and absence of PEARL-seq probes.

#### Basic usage

For instance, `pearlseq --unique --counts stalls example_data_stalls/0_raw example_data_stalls/1_pearlseq --input_sheet example_data_stalls/input_sheet.csv`

When optimizing parameters, we also recommend using `--keep_temp` to retain all intermediate files. These can be manually removed later with `rm -r 1_pearlseq/temp`

### Inputs

The inputs to PEARLseq are described in more detail [here](docs/input_files.md). Here is a quick summary:

- A spreadsheet/CSV file describing the samples, their treatments, and whether they are 'probe' (e.g. NAI, DMS, PEARL-seq probe) or 'control' (e.g. DMSO, PBS, PEARL-seq warhead control)  (input_sheet.csv).
- A FASTA file specifying the reference sequences corresponding to the constructs used in the experiment (ref_seqs.fa)
- A FASTA file specifying sequence masks (with 'X' characters in positions that should be ignored in downstream analysis) (ref_masks.fa)


## Usage
```
usage: pearlseq.py [-h] [--input_sheet INPUT_SHEET] [--ref_fa REF_FA]
                   [--mask_file MASK_FILE]
                   [--pear_params PEAR_PARAMS [PEAR_PARAMS ...]]
                   [--align_method {bowtie2,bowtie2-endToEnd}]
                   [--sort_memory SORT_MEMORY] [--unique]
                   [--count {mutations,stalls,both}]
                   [--pileup {bam-readcount,shapemapper2}]
                   [--min_coverage MIN_COVERAGE] [--max_mut_rate MAX_MUT_RATE]
                   [--max_ctrl_mut_rate MAX_CTRL_MUT_RATE]
                   [--max_indel_length MAX_INDEL_LENGTH]
                   [--min_read_length MIN_READ_LENGTH] [--use_only_pairs]
                   [--skip_insertions] [--skip_deletions]
                   [--num_cores NUM_CORES] [--overwrite] [--dryrun]
                   [--force_incomplete] [--unlock] [--processors PROCESSORS]
                   [--keep_temp]
                   input_dir output_dir

```


### PEARL-seq is a software package for analysis of pearl-seq or rna chemical probing data.
For more information, visit
https//bitbucket.org/arrakistx/pearl-seq

  
### Optional arguments

|Short|Long    |Default|Description                    |
|-----|--------|-------|-------------------------------|
|`-h` |`--help`|       |show this help message and exit|  
  
### Inputs and outputs options
  input_dir             directory where input fastq files are found
  output_dir            directory where output files will be created


|Short|Long           |Default          |Description                                                   |
|-----|---------------|-----------------|--------------------------------------------------------------|
|     |`--input_sheet`|`input_sheet.csv`| Input spreadsheet, if not found in standard location         |
|     |`--ref_fa`     |`ref_seqs.fa`    |FASTA file containing reference sequences                     |
|     |`--mask_file`  |`ref_masks.fa`   | FASTA file describing masked positions in reference sequences|  
  
### Read alignment options


|Short|Long            |Default  |Description                                                                                                                                                                                                                                              |
|-----|----------------|---------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|`-pp`|`--pear_params` |         | String of extra parameters for PEAR stitcher. (default: )                                                                                                                                                                                               |
|     |`--align_method`|`bowtie2`| Alignment method (available: bwa-mem, bowtie2, bowtie2-endToEnd)                                                                                                                                                                                        |
|     |`--sort_memory` |`768M`   | Memory to allocated to samtools sort. Warning: will be multiplied by number of processors for final memory consumption. Needed for very large samples (>25G fastq) in which number of sort files exceeds system limit for opened files (typically 1024) |
|     |`--unique`      |`False`  |retain only uniquely mapping reads (map q > 10)                                                                                                                                                                                                          |  

  
### Mutation profiling options

|Short|Long                 |Default        |Description                                                                                                           |
|-----|---------------------|---------------|----------------------------------------------------------------------------------------------------------------------|
|     |`--count`            |`mutations`    | Types of evidence to count when tabulating reactivity                                                                |
|     |`--pileup`           |`bam-readcount`| Pileup software to use                                                                                               |
|     |`--min_coverage`     |`1000`         | Minimum read coverage to calculate reactivities at a given position                                                  |
|     |`--max_mut_rate`     |`0.2`          | Positions with mutation rates greater than this fraction will be filtered out (0-1).                                 |
|     |`--max_ctrl_mut_rate`|`0.02`         | Positions with mutation rates greater than this fraction will be filtered out (0-1).                                 |
|     |`--max_indel_length` |`10`           | Maximum length of indels to consider for reactivity calculations. Indels with length > this value will be discarded. |
|     |`--min_read_length`  |`30`           | Minimum read length after trimming by base quality.                                                                  |
|     |`--use_only_pairs`   |`False`        |Only use read pairs where both mates survive trimming and QC.                                                         |
|     |`--skip_insertions`  |`False`        |Do not use insertions when calculating reactivity profiles.                                                           |
|     |`--skip_deletions`   |`False`        |Do not use deletions when calculating reactivity profiles.                                                            |  

  
### Snakemake options

|Short|Long                |Default|Description                                                            |
|-----|--------------------|-------|-----------------------------------------------------------------------|
|     |`--num_cores`       |`16`   | Number of cores to use (default: all available)                       |
|     |`--overwrite`       |`False`|Overwrite final output files even if precurors unchanged.              |
|     |`--dryrun`          |`False`|Snakemake dry run                                                      |
|     |`--force_incomplete`|`False`|Snakemake force overwrite incomplete files                             |
|     |`--unlock`          |`False`|Snakemake unlock directory only                                        |
|     |`--processors`      |`1`    | Maximum number of processors to use when multiprocessing is available |
|     |`--keep_temp`       |`False`|Keep temp files                                                        |  

  
## License

PEARLseq is developed at [Arrakis Therapeutics](http://arrakistx.com/) and released under a GPL v3 license.

## Contact

For any questions or comments about PEARLseq, contact Lee Vandivier (lvandivier@arrakistx.com)

