#base docker image: ubuntu
FROM continuumio/miniconda:4.7.12
MAINTAINER Lee Vandivier lvandivier@arrakistx.com

#Set the working directory
USER root

#Install pearlseq dependencies via conda
RUN conda create -y --name pearlseq python=3.6
RUN conda install -y --name pearlseq -c conda-forge \
 pigz=2.3.4 \
 numpy=1.18.1 \
 pandas=1.0.1 \
 seaborn=0.10.0 \
 matplotlib=3.1.3

RUN conda install -y --name pearlseq -c bioconda \
 bam-readcount=0.8 \
 bedtools=2.29.2 \
 samtools=1.6 \
 pear=0.9.6 \
 umi_tools=1.0.1 \
 biopython=1.76 \
 bowtie2=2.3.5.1 \
 pysam=0.15.3 \
 trimmomatic=0.39 \
 cutadapt=1.18 \
 bwa=0.7.17

RUN conda install -y --name pearlseq -c conda-forge -c bioconda snakemake=3.13.3

# Make conda env accessible
RUN echo "source activate pearlseq" > ~/.bashrc
ENV PATH /opt/conda/envs/pearlseq/bin:$PATH

# Copy pearlseq src code
COPY ./pearlseq.py /app/pearlseq.py
COPY ./src/ app/src

WORKDIR /home

ENTRYPOINT [ "python", "/app/pearlseq.py" ]




