#!/usr/bin/env bash

# Helper script to install dependencies for pearlseq

#build docker image, which includes Conda
docker build -t arrakistx/pearlseq .

if grep -q 'alias pearlseq' ~/.bash_profile; then
	echo 'pearlseq alias already in bash_profile'
else
	echo 'Adding pearlseq alias to bash_profile'
	echo -e '\n#added by pearlseq installer:' >> ~/.bash_profile
	echo "alias pearlseq='docker run -u $(id -u ${USER}):$(id -g ${USER}) -v \`pwd\`:/home arrakistx/pearlseq'" >> ~/.bash_profile
	source ~/.bash_profile
fi

echo "Do you want to download the example files? (y/n)"

read dl_option

curl_path=$(command -v curl)
wget_path=$(command -v wget)

if [ $dl_option == 'y' ] || [ $dl_option == 'Y' ]; then

	if [ ! -f $curl_path ]; then
		echo "curl binary not found. curl or wget is required to download files."
		
	elif [ ! -f $wget_path ]; then
		echo "wget binary not found. curl or wget is required to download files."
		exit 1
	else
		example_data_stalls/download_files.sh
		echo "You should now be able to test pearlseq in stall mode with the following commands:"
		echo "cd example_data_stalls"
		echo "pearlseq 0_raw 1_pearlseq --stitch --unique --count stalls"
		example_data_mutations/download_files.sh
		echo "You should now be able to test pearlseq in stall mode with the following commands:"
		echo "cd example_data_stalls"
		echo "pearlseq 0_raw 1_shapemap --unique --count mutations"
	fi
fi
